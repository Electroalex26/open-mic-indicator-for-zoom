# Open mic indicator for Zoom.us

This program check the Zoom's shortcut wich switch on and off the mic and light a bulb when the mic is open.

## Prérequis

* Python3 : ```sudo apt install python3 python3-pip```
* keyboard : ```sudo python3 -m pip install keyboard```

## Configuration

Renommez le fichier de configuration :

```sh
cp config_example.ini config.ini
```

Modifiez ensuite ce fichier pour ajouter votre nom d'utilisateur, le nom des fichiers et leur répertoire.

## Démarrage simple

```sh
sudo python3 omi.py
```

## Automatisation

Tout d'abord il faut rendre le fichier éxecutable pour l'utilisateur :

```sh
chmod u+x omi.py
```

Ensuite, modifiez le fichier ```omi.service``` en y ajoutant votre nom d'utilisateur, nom du groupe, et adresse de l'éxecutable.

Déplacez le ensuite dans le dossier suivant et exécutez les commandes suivantes (avec les droits root) :

```sh
sudo mv omi.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable omi.service
sudo systemctl start omi.service
```

Vous pouvez vérifier son bon fonctionnement en exécutant la commande suivante :

```sh
sudo systemctl status omi.service
```

