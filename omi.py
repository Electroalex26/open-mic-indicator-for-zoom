#!/bin/python3

import keyboard
from time import sleep
import requests
from os import system
from os import path as os_path
import configparser
from datetime import datetime

# RECUPERATION CONFIGURATION
config = configparser.ConfigParser()
PATH = os_path.abspath(os_path.split(__file__)[0])
config.read(PATH + "/config.ini")
dir = config['FILES']['dir']
if not dir.endswith("/"):
   dir = dir + "/"

# DEFINITION FONCTIONS
#   Affiche un message de log avec horodatage et l'enregistre dans un fichier défini
def log(message):
   global config
   file = open(dir + config['FILES']['log_file'], "a")
   file.write(datetime.now().strftime("%d/%m/%Y %H:%M") + " : " + message + "\n")
   file.close()
   print(datetime.now().strftime("%d/%m/%Y %H:%M") + " : " + message)

# DECLARATION VARIABLES
state = False
zoom_open = False
i = int(config['CONST']['check_delay']) * 100 + 1

# PROGRAMME
while True:
    # CHECK IF ZOOM IS OPEN
    if i > int(config['CONST']['check_delay']) * 100:
        system("ps -u " + config['USER']['zoom_user'] + " > /tmp/ps")
        file = open("/tmp/ps", "r")
        raw = file.read()
        file.close()
        if "zoom" in raw:
            zoom_open = True
        else:
            zoom_open = False
        i = 0
        log("zoom_open : " + str(zoom_open))
    i += 1
    # IF ZOOM IS OPEN AND SHORTCUT PRESS, CHANGE STATE
    if keyboard.is_pressed('a') and keyboard.is_pressed(56) and zoom_open:
        log("shortcut detected")
        state = not state
        if state:
            r = requests.get(config['URLS']['url_on'])
            log("bulb on")
        else:
            r = requests.get(config['URLS']['url_off'])
            log("bulb off")
        while keyboard.is_pressed('a') and keyboard.is_pressed(56):
            state = state
    elif not zoom_open:
        r = requests.get(config['URLS']['url_off'])
        log("error, zoom is closed")
    sleep(0.010)